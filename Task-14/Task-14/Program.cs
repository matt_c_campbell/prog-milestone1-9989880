﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*

    Author:         Matt. Campbell
    Date Created:   23rd August, 2016.

    Task 14:        'Convert advertised to computer storage capacity.'

*/

namespace Task_14
{
    class Program
    {
        static void Main(string[] args)
        {
            string input_s;
            double input_d, pc_size;
            bool correct_format = false;

            do
            {
                Console.Clear();

                Console.WriteLine("What is your device's advertised storage capacity in GB? Type in an Integer (whole number) below:");
                input_s = Console.ReadLine();
                double.TryParse(input_s, out input_d);

                if (input_d <= 0)
                {
                    Console.WriteLine("\n>    Invalid input! Try typing in an Integer (whole number) higher than 0.");

                    Console.WriteLine("\n\n-----------------------------------------------------------------------------------------------------------------------");

                    Console.WriteLine("Press [Enter] to try again . . .");
                    Console.ReadLine();
                }
                else
                {
                    correct_format = true;
                }
            } while (correct_format == false);

            Console.Clear();

            pc_size = ((input_d / 1.024) / 1.024) / 1.024;
            pc_size = Math.Round(pc_size, 2);
            Console.WriteLine($"Advertised Storage Capacity: \t{input_d} GB \nPC Storage Capacity: \t\t{pc_size} GB");

            Console.WriteLine("\n\n-----------------------------------------------------------------------------------------------------------------------");

            Console.WriteLine("Press [Enter] to exit program . . .");
            Console.ReadLine();
        }
    }
}
