﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*

    Author:         Matt. Campbell
    Date Created:   22nd August, 2016.

    Task-06:        'Forloop - calculate the sum of 5 numbers to "n".'

*/

namespace Task_06
{
    class Program
    {
        static void Main(string[] args)
        {
            int max_repetitions = 5, input_i, users_numbers = 0;
            string input_s;
            bool correct_format = false;

            for (var i = 0; i < max_repetitions; i++)
            {
                do
                {
                    Console.Clear();
                    var a = i + 1;

                    Console.WriteLine($"Type in your {a} Integer (whole number):");
                    input_s = Console.ReadLine();
                    int.TryParse(input_s, out input_i);

                    if (input_i == 0)
                    {
                        Console.WriteLine("\n>    Invalid input! Type in an Integer (whole number) that does not equal 0.");
                        Console.WriteLine("\n\n-----------------------------------------------------------------------------------------------------------------------");
                        Console.WriteLine("Press [Enter] to try again . . .");
                        Console.ReadLine();
                        Console.Clear();
                    }
                    else
                    {
                        users_numbers += input_i;

                        if (a <= 4)
                        {
                            Console.WriteLine($"\n>    Your {a} numbers currently add up to {users_numbers}.");
                            Console.WriteLine("\n\n-----------------------------------------------------------------------------------------------------------------------");
                            Console.WriteLine("Press [Enter] to continue . . .");
                            Console.ReadLine();
                        }
                        else if (a == 5)
                        {
                            Console.WriteLine($"\n>    Your 5 numbers add up to {users_numbers}.");
                            Console.WriteLine("\n\n-----------------------------------------------------------------------------------------------------------------------");
                            Console.WriteLine("Press [Enter] to exit program . . .");
                            Console.ReadLine();
                        }


                        correct_format = true;
                    }
                } while (correct_format == false);

                correct_format = false;
            }
        }
    }
}
