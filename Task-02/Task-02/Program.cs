﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*

    Author: Matt. Campbell
    Date Created: 22nd August, 2016.

    Task 02: 'Get user input (birth month & day) and display it in a sentance.'

*/

namespace Task_02
{
    class Program
    {
        static void Main(string[] args)
        {
            var months = new string[13] {"", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
            string birthmonth_s, birthday_s;
            int birthmonth_i, birthday_i;
            bool correct_format1 = false;
            bool correct_format2 = false;

            // Continually ask for user's birth month until 'birthmonth_i' is valid.
            do
            {
                Console.WriteLine("What month were you born in? Please type in a number from below:");
                Console.WriteLine($"\n[ 1] {months[1]}\t\t[ 7] {months[7]}  \n[ 2] {months[2]}\t\t[ 8] {months[8]}  \n[ 3] {months[3]}\t\t[ 9] {months[9]}  \n[ 4] {months[4]}\t\t[10] {months[10]}  \n[ 5] {months[5]}\t\t[11] {months[11]}  \n[ 6] {months[6]}\t\t[12] {months[12]}");
                birthmonth_s = Console.ReadLine();
                int.TryParse(birthmonth_s, out birthmonth_i);

                if (birthmonth_i <= 0 || birthmonth_i >= 13)
                {
                    Console.WriteLine("\n>    Invalid input! Try typing in an Integer (whole number) between 1 and 12.");

                    Console.WriteLine("\n\n-----------------------------------------------------------------------------------------------------------------------");
                    Console.WriteLine("Press [Enter] to try again . . .");
                    Console.ReadLine();

                    Console.Clear();
                } else {
                    correct_format1 = true;
                }
            } while (correct_format1 == false);

            Console.WriteLine("\n\n-----------------------------------------------------------------------------------------------------------------------\n\n");

            // Continually ask for user's birth day until 'birthday_i' is valid.
            do
            {
                Console.WriteLine("What day were you born on? Please type in an Integer (whole number) between 1 and 31:");
                birthday_s = Console.ReadLine();
                int.TryParse(birthday_s, out birthday_i);

                if (birthday_i <= 0 || birthday_i >= 32)
                {
                    Console.WriteLine("\n>    Invalid input! Try typing in an Integer (whole number) between 1 and 31.");

                    Console.WriteLine("\n\n-----------------------------------------------------------------------------------------------------------------------");
                    Console.WriteLine("Press [Enter] to try again . . .");
                    Console.ReadLine();

                    Console.Clear();
                } else {

                    // Pause program on-screen until user is ready to exit.
                    Console.WriteLine($"\n>    You were born on the {birthday_i} of {months[birthmonth_i]}.");

                    Console.WriteLine("\n\n-----------------------------------------------------------------------------------------------------------------------");
                    Console.WriteLine("Press [Enter] to exit program . . .");
                    Console.ReadLine();

                    correct_format2 = true;
                }
            } while (correct_format2 == false);
        }
    }
}
