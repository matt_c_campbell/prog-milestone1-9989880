﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*

    Author:         Matt. Campbell
    Date Created:   23rd August, 2016.

    Task 34:        'How many working days when user enters number of weeks.'

*/

namespace Task_34
{
    class Program
    {
        static void Main(string[] args)
        {
            bool correct_format = false;
            string weeks_s;
            int weeks_i, week_days, work_days = 0;

            do
            {
                Console.WriteLine("How many working days are in X weeks? Enter an Integer (whole number) to find out:");
                weeks_s = Console.ReadLine();
                int.TryParse(weeks_s, out weeks_i);

                if (weeks_i <= 0)
                {
                    Console.WriteLine("\n>    Invalid week number. Try an Integer (whole number) higher than 0.");

                    Console.WriteLine("\n\n-----------------------------------------------------------------------------------------------------------------------");

                    Console.WriteLine("Press [Enter] to try again . . .");
                    Console.ReadLine();

                    Console.Clear();
                }
                else
                {
                    // Converting weeks to week days.
                    week_days = weeks_i * 7;
                    work_days = week_days - (2 * weeks_i);

                    correct_format = true;
                }
            } while (correct_format == false);

            Console.Clear();

            Console.WriteLine($">    There are {work_days} working days in {weeks_i} weeks.");

            Console.WriteLine("\n\n-----------------------------------------------------------------------------------------------------------------------");

            Console.WriteLine("Press [Enter] to exit program");
            Console.ReadLine();
        }
    }
}
