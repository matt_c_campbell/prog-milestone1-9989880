﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*

    Author:         Matt. Campbell
    Date Created:   23rd August, 2016.

    Task 27:        'Create an array and print it to the screen in ascending order.'

*/

namespace Task_27
{
    class Program
    {
        static void Main(string[] args)
        {
            var colors = new string[5] { "Red", "Blue", "Yellow", "Green", "Pink" };
            string colors_unsorted, colors_sorted;


            colors_unsorted = string.Join(", ", colors);
            Array.Sort(colors);
            colors_sorted = string.Join(", ", colors);

            Console.WriteLine($"Unsorted Colors: \t{colors_unsorted}\n\nSorted Colors: \t\t{colors_sorted}");

            Console.WriteLine("\n\n-----------------------------------------------------------------------------------------------------------------------");

            Console.WriteLine("Press [Enter] to exit program . . .");
            Console.ReadLine();
        }
    }
}
