﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*

    Author:         Matt. Campbell
    Date Created:   23rd August, 2016.

    Task 25:        'Program takes user input for a number and cannot crash.'

*/

namespace Task_25
{
    class Program
    {
        static void Main(string[] args)
        {
            bool correct_format = false;
            string input_s;
            int input_i;

            do
            {
                Console.Clear();

                Console.WriteLine("Type in an Integer (whole number):");
                input_s = Console.ReadLine();
                int.TryParse(input_s, out input_i);

                if (input_i == 0)
                {
                    Console.WriteLine("\n>    Invalid number! Try typing in an Integer (whole number) that is not equal to 0.");

                    Console.WriteLine("\n\n-----------------------------------------------------------------------------------------------------------------------");

                    Console.WriteLine("Press [Enter] to try again . . .");
                    Console.ReadLine();

                    Console.Clear();
                }
                else
                {
                    correct_format = true;
                }
            } while (correct_format == false);

            Console.Clear();

            Console.WriteLine($"Your number is {input_i}.");

            Console.WriteLine("\n\n-----------------------------------------------------------------------------------------------------------------------");

            Console.WriteLine("Press [Enter] to exit program . . .");
            Console.ReadLine();
        }
    }
}
