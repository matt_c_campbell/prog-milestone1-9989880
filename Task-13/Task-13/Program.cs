﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*

    Author:         Matt. Campbell
    Date Created:   23 August, 2016.

    Task 13:        'Mini Shop - Take the sum of 3 doubles (user input) and add 15% GST.'

*/

namespace Task_13
{
    class Program
    {
        static void Main(string[] args)
        {
            const double GST = 1.15;
            var repetitions = 3;
            string input_s;
            double input_d, users_prices = 0, final_price = 0, gst_amount = 0;
            bool correct_format = false;

            for (var i = 0; i < repetitions; i++)
            {
                var a = i + 1;
                
                do
                {
                    Console.Clear();

                    Console.WriteLine($"Type in your {a} item's price:");
                    input_s = Console.ReadLine();
                    double.TryParse(input_s, out input_d);

                    if (input_d <= 0)
                    {
                        Console.WriteLine("\n>    Invalid input! Try entering a price (number) higher than 0.");

                        Console.WriteLine("\n\n-----------------------------------------------------------------------------------------------------------------------");
                        Console.WriteLine("Press [Enter] to try again . . .");
                        Console.ReadLine();

                        Console.Clear();
                    }
                    else
                    {
                        users_prices += input_d;
                        correct_format = true;
                    }
                } while (correct_format == false);

                correct_format = false;
            }

            Console.Clear();

            final_price = users_prices * GST;
            gst_amount = final_price - users_prices;

            Console.WriteLine($"Total:\t\t${users_prices}  \nGST:\t\t${gst_amount}  \n\n--------------------------------------  \nTotal + GST:\t${final_price}");

            Console.WriteLine("\n\nPress [Enter] to exit program . . .");
            Console.ReadLine();
        }
    }
}
