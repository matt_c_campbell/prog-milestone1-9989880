﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*

    Author:         Matt. Campbell
    Date Created:   22nd August, 2016.
    
    Task 01:        'Get user input (name & age) and 3 ways to print a string.'

*/

namespace Task_01
{
    class Program
    {
        static void Main(string[] args)
        {
            string name, age_s;
            int age_i;

            Console.WriteLine("What is your name? Please type it in below:");
            name = Console.ReadLine();
            // Print 'name' to confirm that it is storing user input.
            // Console.WriteLine($"\n>    Your name is {name}.");

            Console.WriteLine($"\nHow old are you, {name}? Please type in your age as an Integer (whole number) below:");
            age_s = Console.ReadLine();
            int.TryParse(age_s, out age_i);
            // Print 'age_i' to confirm that string input was converted to int value.
            Console.WriteLine($"\n>    {name} is {age_i} years old.");

            Console.WriteLine("\n\n-----------------------------------------------------------------------------------------------------------------------\n\n");

            Console.WriteLine("3 Ways to Print String Variable:");
            Console.WriteLine("\n     String Concatenation");
            Console.WriteLine(">    Console.WriteLine(\"The time is \" + current_time + \" and I am wearing \" + current_clothing + \" .\");");
            Console.WriteLine("\n     Positional Formatting");
            Console.WriteLine(">    Console.WriteLine(\"My favourite song is {0} by {1}.\", fav_song, band_name);");
            Console.WriteLine("\n     String Interpolation");
            Console.WriteLine(">    Console.WriteLine($\"I like {fav_color} eggs and ham on {fav_weekday}.\");");

            Console.WriteLine("\n\n-----------------------------------------------------------------------------------------------------------------------\n\n");

            // Pause program on-screen until user is ready to exit.
            Console.WriteLine("Press [Enter] to exit program . . .");
            Console.ReadLine();
        }
    }
}
