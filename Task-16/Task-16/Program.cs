﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*

    Author:         Matt. Campbell
    Date Created:   23rd August, 2016.

    Task 16:        'Count the letters in a word (dislay the string and its character length).'

*/

namespace Task_16
{
    class Program
    {
        static void Main(string[] args)
        {
            string input;
            int char_count;

            Console.WriteLine("Faceroll the keyboard and then press [Enter]:");
            input = Console.ReadLine();

            char_count = input.Length;

            Console.Clear();

            Console.WriteLine($"The string {input} contains {char_count} characters.");

            Console.WriteLine("\n\n-----------------------------------------------------------------------------------------------------------------------");

            Console.WriteLine("Press [Enter] to exit program . . .");
            Console.ReadLine();
        }
    }
}
