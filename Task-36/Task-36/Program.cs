﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*

    Author:         Matt. Campbell
    Date Created:   23rd August, 2016.

    Task 36:        'Forloop with counter. If statement gives feedback on whether or not counter is equal to index. Different feedback when loop is complete.'

*/

namespace Task_36
{
    class Program
    {
        static void Main(string[] args)
        {
            var repetitons = 20;

            for (var i = 0; i < repetitons; i++)
            {
                var a = i + 1;

                if (i < repetitons)
                {
                    Console.WriteLine($"This is line {a}");
                }
            }

            Console.WriteLine($"\nThe For.. loop above is now complete.");

            Console.WriteLine("\n\n-----------------------------------------------------------------------------------------------------------------------");

            Console.WriteLine("Press [Enter] to exit program . . .");
            Console.ReadLine();
        }
    }
}
