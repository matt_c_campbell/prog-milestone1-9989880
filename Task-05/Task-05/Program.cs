﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*

    Author:         Matt. Campbell
    Date Created:   22nd August, 2016.

    Task 05:        'Convert 24-hour time to 12-hour time.'

*/

namespace Task_05
{
    class Program
    {
        static void Main(string[] args)
        {
            string time_s;
            int time_i;
            bool correct_format = false;

            do
            {
                Console.WriteLine("Please enter a 24-hour time (whole number between 1 and 24):");
                time_s = Console.ReadLine();
                int.TryParse(time_s, out time_i);

                if (time_i <= 0 || time_i >= 25)
                {
                    Console.WriteLine("\n>    Invalid input! Try typing in an Integer (whole number) between 1 and 24");

                    Console.WriteLine("\n\n-----------------------------------------------------------------------------------------------------------------------");
                    Console.WriteLine("Press [Enter] to try again . . .");
                    Console.ReadLine();
                    Console.Clear();
                }

                if (time_i >= 13 && time_i <= 23)
                {
                    time_i -= 12;

                    Console.WriteLine($"\n>    The time is {time_i} p.m.");
                    correct_format = true;
                }
                else if (time_i >= 1 && time_i <= 11)
                {
                    Console.WriteLine($"\n>    The time is {time_i} a.m.");
                    correct_format = true;
                }

                if (time_i == 24)
                {
                    Console.WriteLine("\n>    The time is 12 a.m.");
                    correct_format = true;
                }
                else if (time_i == 12)
                {
                    Console.WriteLine("\n>    The time is 12 p.m.");
                    correct_format = true;
                }
            } while (correct_format == false);

            Console.WriteLine("\n\n-----------------------------------------------------------------------------------------------------------------------");
            Console.WriteLine("Press [Enter] to exit program . . .");
            Console.ReadLine();
        }
    }
}
