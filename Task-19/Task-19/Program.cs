﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*

    Author:         Matt. Campbell
    Date Created:   23rd August, 2016.

    Task-19:        'Create a List<T> that holds people's name and age.'

*/

namespace Task_19
{
    class Program
    {
        static void Main(string[] args)
        {
            var info = new List<Tuple<string, int>>();
            string temp1, temp2_s;
            int repetitions = 3, temp2_i;
            bool correct_format = false;

            for (var i = 0; i < repetitions; i++)
            {
                var a = i + 1;
                Console.Clear();

                Console.WriteLine($"Please enter {a} person's name:");
                temp1 = Console.ReadLine();

                do
                {
                    Console.WriteLine($"\nPlease enter {a} person's age:");
                    temp2_s = Console.ReadLine();
                    int.TryParse(temp2_s, out temp2_i);

                    if (temp2_i <= 0)
                    {
                        Console.WriteLine("\n>    Invalid age! Type in an Integer (whole number) higher than 0.");

                        Console.WriteLine("\n\n-----------------------------------------------------------------------------------------------------------------------");
                        Console.WriteLine("Press [Enter] to try again . . .");
                        Console.ReadLine();

                        Console.Clear();
                    }
                    else
                    {
                        info.Add(Tuple.Create(temp1, temp2_i));
                        correct_format = true;
                    }
                } while (correct_format == false);

                correct_format = false;
            }

            Console.Clear();

            for (var i = 0; i < repetitions; i++)
            {
                Console.WriteLine($"{info[i].Item1} is {info[i].Item2} years old.\n");
            }

            Console.WriteLine("\n-----------------------------------------------------------------------------------------------------------------------");

            Console.WriteLine("Press [Enter] to exit program . . .");
            Console.ReadLine();
        }
    }
}
